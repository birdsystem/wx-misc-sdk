<?php

namespace WarehouseX\Misc\Model\Attachment;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Attachment.
 */
class AttachmentInput extends AbstractModel
{
    /**
     * @var string|null
     */
    public $suffix = null;

    /**
     * @var string
     */
    public $fileType = null;

    /**
     * @var string
     */
    public $recordType = null;

    /**
     * @var int|null
     */
    public $recordId = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string|null
     */
    public $base64Binary = null;
}
