<?php

namespace WarehouseX\Misc\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Attachment.
 */
class Attachment extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string|null
     */
    public $suffix = null;

    /**
     * @var string
     */
    public $fileType = 'MISC';

    /**
     * @var string
     */
    public $recordType = 'NOTICE';

    /**
     * @var int
     */
    public $recordId = null;

    /**
     * @var string|null
     */
    public $contextPath = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string|null
     */
    public $createTime = null;

    /**
     * @var string|null
     */
    public $fullPath = null;
}
