<?php

namespace WarehouseX\Misc;

use OpenAPI\Runtime\ResponseTypes as AbstractResponseTypes;

class ResponseTypes extends AbstractResponseTypes
{
    public array $types = [
        'getAttachmentCollection' => [
            '200.' => 'WarehouseX\\Misc\\Model\\Attachment\\Output[]',
        ],
        'postAttachmentCollection' => [
            '201.' => 'WarehouseX\\Misc\\Model\\Attachment\\Output',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getAttachmentItem' => [
            '200.' => 'WarehouseX\\Misc\\Model\\Attachment\\Output',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putAttachmentItem' => [
            '200.' => 'WarehouseX\\Misc\\Model\\Attachment\\Output',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteAttachmentItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchAttachmentItem' => [
            '200.' => 'WarehouseX\\Misc\\Model\\Attachment\\Output',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'downloadAttachmentItem' => [
            '200.' => 'WarehouseX\\Misc\\Model\\Attachment\\Output',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
    ];
}
