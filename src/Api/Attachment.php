<?php

namespace WarehouseX\Misc\Api;

use WarehouseX\Misc\Model\Attachment as AttachmentModel;
use WarehouseX\Misc\Model\Attachment\AttachmentInput as AttachmentInput;
use WarehouseX\Misc\Model\Attachment\Output as Output;

class Attachment extends AbstractAPI
{
    /**
     * Retrieves the collection of Attachment resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'title'	string
     *                       'suffix'	string
     *                       'fileType'	string
     *                       'fileType[]'	array
     *                       'recordType'	string
     *                       'recordType[]'	array
     *                       'recordId'	integer
     *                       'recordId[]'	array
     *                       'note'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[title]'	string
     *                       'order[suffix]'	string
     *                       'order[fileType]'	string
     *                       'order[recordType]'	string
     *
     * @return Output[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getAttachmentCollection',
        'GET',
        'api/misc/attachments',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Attachment resource.
     *
     * @param AttachmentInput $Model The new Attachment resource
     *
     * @return Output
     */
    public function postCollection(AttachmentInput $Model): Output
    {
        return $this->request(
        'postAttachmentCollection',
        'POST',
        'api/misc/attachments',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a Attachment resource.
     *
     * @param string $id Resource identifier
     *
     * @return Output|null
     */
    public function getItem(string $id): ?Output
    {
        return $this->request(
        'getAttachmentItem',
        'GET',
        "api/misc/attachments/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Attachment resource.
     *
     * @param string          $id    Resource identifier
     * @param AttachmentModel $Model The updated Attachment resource
     *
     * @return Output
     */
    public function putItem(string $id, AttachmentModel $Model): Output
    {
        return $this->request(
        'putAttachmentItem',
        'PUT',
        "api/misc/attachments/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Attachment resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteAttachmentItem',
        'DELETE',
        "api/misc/attachments/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the Attachment resource.
     *
     * @param string $id Resource identifier
     *
     * @return Output
     */
    public function patchItem(string $id): Output
    {
        return $this->request(
        'patchAttachmentItem',
        'PATCH',
        "api/misc/attachments/$id",
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a Attachment resource.
     *
     * @param string $id Resource identifier
     *
     * @return Output|null
     */
    public function downloadItem(string $id): ?Output
    {
        return $this->request(
        'downloadAttachmentItem',
        'GET',
        "api/misc/attachments/$id/download",
        null,
        [],
        []
        );
    }
}
